package com.company;

import com.company.models.Otp;
import com.company.models.Question;
import com.company.models.User;
import com.company.repository.BooleanQuestionCollection;
import com.company.repository.OptionalQuestionCollection;
import com.company.repository.TextQuestionCollection;
import com.company.service.OtpService;
import com.company.service.UserService;
import com.company.utils.InputUtils;
import com.company.utils.MobileUtils;
import com.company.utils.ValidationUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

public class Main {
    private static UserService userService = new UserService();
    private static TextQuestionCollection textQuestionCollection = new TextQuestionCollection();
    private static OptionalQuestionCollection optionalQuestionCollection = new OptionalQuestionCollection();
    private static BooleanQuestionCollection booleanQuestionCollection = new BooleanQuestionCollection();
    private static OtpService otpService = new OtpService();

    private static Long phoneNumber;

    public static void main(String[] args) {
        showQuestion(textQuestionCollection.getQuestionForNewUser().get(0));
        showMessage("Enter Your Phone Number");

        try {
            final String phn = InputUtils.getNextString();
            if (!MobileUtils.isValid(phn)) {
                showMessage("Phone Number Not Valid, Please Try Again !!");
                return;
            }
            phoneNumber = Long.valueOf(phn);
            final boolean existingUser = userService.isExistingUser(phoneNumber);
            if (existingUser) {
                existingUserFlow(); // existing user
            } else {
                // new user
                final int id = userService.countUsers() + 1;
                final User newUser = new User();
                newUser.setId(id);
                newUser.setPhoneNumber(phoneNumber);
                newUser.setBalance(0.0);
                newUser.setPassword(InputUtils.generatePassword());
                userService.createUser(newUser);

                newUserOtpFlow(id);
            }
        } catch (Exception e) {
            System.out.println(ExceptionUtils.getRootCauseMessage(e));
            exit();
        }
    }

    private static void existingUserFlow() {
        showQuestion(optionalQuestionCollection.getQuestionForExistingUser().get(0));
        final int i = InputUtils.getNextInt();
        if (i == 1) {
            final double balance = userService.getBalance(phoneNumber);
            showMessage("Your balance is " + balance);

            if (balance < 200) {
                showQuestion(booleanQuestionCollection.getQuestionForExistingUser().get(0));
                final String rechargeOption = InputUtils.getNextString();
                if (rechargeOption.equals("yes") || rechargeOption.equals("YES") || rechargeOption.equals("Yes")) {
                    recharge(phoneNumber);
                } else {
                    exit();
                }
            } else {
                showMessage("Press OK to exit");
                if (InputUtils.getNextString().equals("OK")) {
                    exit();
                }
            }
        } else {
            recharge(phoneNumber);
        }
    }

    private static void newUserOtpFlow(final Integer id) {
        final int otpGen = InputUtils.generateOtp();
        final Otp otp = new Otp(otpGen, id);
        showMessage("OTP is sent to you, your OTP is => " + otp.getOtpNumber());
        otpService.create(otp);
        showMessage("Please Enter Otp to Continue");
        final int userOtp = InputUtils.getNextInt();
        final boolean otpWrong = otpService.isOtpWrong(userOtp, id);
        if (otpWrong) {
            //wrong otp
            showQuestion(optionalQuestionCollection.getQuestionForNewUser().get(0));
            int inputOtpChoice = InputUtils.getNextInt();
            if (inputOtpChoice == 1) {
                // renter otp
                showMessage("ReEnter OTP");
                int reEnterOtp = InputUtils.getNextInt();
                while (reEnterOtp != otp.getOtpNumber()) {
                    showMessage("Incorrect OTP, Enter OTP again !!");
                    reEnterOtp = InputUtils.getNextInt();
                }
                existingUserFlow();
            } else if (inputOtpChoice == 2) {
                newUserOtpFlow(id);  // regenerate otp
            } else {
                exit();
            }
        } else {
            // correct otp
            existingUserFlow();
        }
    }

    private static void recharge(final Long phnNumber) {
        showQuestion(textQuestionCollection.getQuestionForExistingUser().get(0));
        final int option = InputUtils.getNextInt();
        double recharge = 0;
        if (option == 1) {
            recharge = 400;
        } else if (option == 2) {
            recharge = 600;
        } else if (option == 3) {
            showMessage("Enter Amount for recharge (>0 <10000)");
            recharge = InputUtils.getNextDouble();
            if (!ValidationUtils.isRechargeValid(recharge)) {
                showMessage("Recharge Value Not Correct, please try again !!");
                exit();
            }
        } else {
            showMessage("Wrong Option Chosen, Please try again !!");
            exit();
        }
        userService.increaseBalance(recharge, phnNumber);
        showMessage("Recharge of " + recharge + " is successful on " + phnNumber);
    }


    private static void showQuestion(final Question question) {
        showMessage(question.toString());
    }

    private static void exit() {
        showMessage("EXIT");
        System.exit(0);
    }

    private static void showMessage(final String message) {
        System.out.println(message);
    }

}
