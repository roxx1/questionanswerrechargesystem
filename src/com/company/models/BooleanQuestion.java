package com.company.models;

import java.util.Arrays;

public class BooleanQuestion implements Question {

    private final Integer id;
    private final String question;
    private static final String[] options = {"yes", "no"};

    public BooleanQuestion(final Integer id, final String question) {
        this.id = id;
        this.question = question;
    }

    public Integer getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public static String[] getOptions() {
        return options;
    }

    @Override
    public String toString() {
        return "{" +
                "'" + question + '\'' +
                ", \n options=" + Arrays.toString(options) +
                '}';
    }
}
