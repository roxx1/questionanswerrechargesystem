package com.company.models;

public class Otp {

    private Integer id;
    private Integer otpNumber;
    private Integer userId;

    public Otp(Integer otpNumber, Integer userId) {
        this.otpNumber = otpNumber;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOtpNumber() {
        return otpNumber;
    }

    public void setOtpNumber(Integer otp) {
        this.otpNumber = otp;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
