package com.company.models;

public class TextQuestion implements Question {

    private final Integer id;
    private final String question;

    public TextQuestion(final Integer id, final String question) {
        this.id = id;
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    @Override
    public String toString() {
        return "{" +
                "'" + question + '\'' +
                '}';
    }
}
