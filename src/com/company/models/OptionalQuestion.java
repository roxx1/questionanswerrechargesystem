package com.company.models;

import java.util.Arrays;

public class OptionalQuestion implements Question {
    private final Integer id;
    private final String question;
    private final String[] options;

    public OptionalQuestion(final Integer id, final String question, String[] options) {
        this.id = id;
        this.question = question;
        this.options = options;
    }

    public Integer getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getOptions() {
        return options;
    }

    @Override
    public String toString() {
        return "{" +
                "'" + question + '\'' +
                ",\n options=" + Arrays.toString(options) +
                '}';
    }
}
