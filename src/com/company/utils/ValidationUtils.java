package com.company.utils;

public final class ValidationUtils {

    private ValidationUtils() {
        throw new AssertionError();
    }

    public static boolean isRechargeValid(final double recharge) {
        return (recharge > 0 && recharge < 10000);
    }

}
