package com.company.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Scanner;

public final class InputUtils {

    private static Scanner in = new Scanner(System.in);

    private InputUtils() {
        throw new AssertionError();
    }

    public static String getNextString() {
        return in.next();
    }

    public static int getNextInt() {
        return in.nextInt();
    }

    public static double getNextDouble() {
        return in.nextDouble();
    }

    public static int generateOtp() {
        return (int) (Math.random() * 9000) + 1000; //[1000-9999]
    }

    public static String generatePassword() {
        int length = 5;
        boolean useLetters = true;
        boolean useNumbers = false;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }
}
