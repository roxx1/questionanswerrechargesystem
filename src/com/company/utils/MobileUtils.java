package com.company.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class MobileUtils {

    private MobileUtils() {
        throw new AssertionError();
    }

    public static boolean isValid(final String phnNumber) {
        final Pattern p = Pattern.compile("[7-9][0-9]{9}");
        final Matcher m = p.matcher(phnNumber);
        return (m.find() && m.group().equals(phnNumber));
    }
}
