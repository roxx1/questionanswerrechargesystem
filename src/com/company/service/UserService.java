package com.company.service;

import com.company.exception.UserNotFoundException;
import com.company.models.User;
import com.company.repository.UserRepository;

import java.util.List;

public class UserService {

    private UserRepository userRepository = new UserRepository();

    public double getBalance(final Long phoneNumber) {
        final User existingUser = getUserFromListWithPhoneNumber(userRepository.getExistingUsers(), phoneNumber);
        checkUserExists(existingUser);
        return existingUser.getBalance();
    }

    public void increaseBalance(final double balance, final Long phoneNumber) {
        isExistingUser(phoneNumber);
        final User user1 = getUserFromListWithPhoneNumber(userRepository.getExistingUsers(), phoneNumber);
        checkUserExists(user1);
        user1.setBalance(user1.getBalance() + balance);
    }

    public boolean isExistingUser(final Long phoneNumber) {
        final User user1 = getUserFromListWithPhoneNumber(userRepository.getExistingUsers(), phoneNumber);
        return user1 != null;
    }


    public void createUser(final User user) {
        userRepository.createUser(user);
    }

    public boolean removeUser(final Long phoneNumber) {
        final User user1 = getUserFromListWithPhoneNumber(userRepository.getExistingUsers(), phoneNumber);
        checkUserExists(user1);
        return userRepository.removeUser(user1);
    }

    public int countUsers() {
        final List<User> existingUsers = userRepository.getExistingUsers();
        if (existingUsers != null) {
            return existingUsers.size();
        }
        return 0;
    }

    private void checkUserListExists(final List<User> list) {
        if (list == null || list.isEmpty()) {
            throw new UserNotFoundException("User Not Found with Given phn no");
        }
    }

    private void checkUserExists(final User user) {
        if (user == null) {
            throw new UserNotFoundException("User Not Found with Given phn no");
        }
    }

    private User getUserFromListWithPhoneNumber(final List<User> userList, final Long phoneNumber) {
        checkUserListExists(userList);
        return userList.stream().filter(user -> user.getPhoneNumber().equals(phoneNumber)).findFirst().orElse(null);
    }
}
