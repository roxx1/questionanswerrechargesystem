package com.company.service;

import com.company.models.Otp;
import com.company.repository.OtpRepository;

public class OtpService {

    private OtpRepository otpRepository = new OtpRepository();

    public void create(final Otp otp) {
        otpRepository.createOtp(otp);
    }

    public boolean isOtpWrong(final int otp, final Integer userId) {
        return otpRepository.isOtpWrong(otp, userId);
    }

    public void updateOtp(final int newOtp, final Integer userId) {
        otpRepository.updateOtp(newOtp, userId);
    }


}
