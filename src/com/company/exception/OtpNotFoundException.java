package com.company.exception;

public class OtpNotFoundException extends RuntimeException {
    public OtpNotFoundException() {
        super();
    }

    public OtpNotFoundException(String message) {
        super(message);
    }

    public OtpNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public OtpNotFoundException(Throwable cause) {
        super(cause);
    }
}
