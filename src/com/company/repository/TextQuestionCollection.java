package com.company.repository;

import com.company.models.TextQuestion;

import java.util.Arrays;
import java.util.List;

public class TextQuestionCollection implements IQuestionCollection<TextQuestion> {

    private static final TextQuestion t1 = new TextQuestion(1,"Q: Welcome to Easy Recharge. Please enter your phone number to continue.");
    private static final TextQuestion t2 = new TextQuestion(2,"Please Enter your OTP sent to you");
    private static final TextQuestion t3 = new TextQuestion(3,"We have following bundles you can use, choose one\n" +
            "1. 50p for 1 minute call. 400 Rs per month\n" +
            "2. 25p for 1 minute call. 600 Rs per month\n" +
            "3. 1Rs for 1 minute call. Recharge arbitrary money");

    @Override
    public List<TextQuestion> getQuestionForExistingUser() {
        return Arrays.asList(t3);
    }

    @Override
    public List<TextQuestion> getQuestionForNewUser() {
        return Arrays.asList(t1,t2);
    }

}
