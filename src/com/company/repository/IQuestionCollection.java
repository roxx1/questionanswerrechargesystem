package com.company.repository;

import com.company.models.Question;

import java.util.List;

public interface IQuestionCollection<T extends Question> {
    List<T> getQuestionForExistingUser();
    List<T> getQuestionForNewUser();
}
