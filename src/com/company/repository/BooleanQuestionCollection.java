package com.company.repository;

import com.company.models.BooleanQuestion;

import java.util.Arrays;
import java.util.List;

public class BooleanQuestionCollection implements IQuestionCollection<BooleanQuestion> {

    private static final BooleanQuestion b1 = new BooleanQuestion(1, "Would you like to recharge?");


    @Override
    public List<BooleanQuestion> getQuestionForExistingUser() {
        return Arrays.asList(b1);
    }

    @Override
    public List<BooleanQuestion> getQuestionForNewUser() {
        return Arrays.asList();
    }
}
