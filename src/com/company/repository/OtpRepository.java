package com.company.repository;

import com.company.exception.OtpNotFoundException;
import com.company.models.Otp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class OtpRepository {

    private static final Otp otp1 = new Otp(1234, 1);
    private static final Otp otp2 = new Otp(3456, 2);
    private static final Otp otp3 = new Otp(6789, 3);
    private static List<Otp> existingOtpList = new ArrayList<>(Arrays.asList(otp1, otp2, otp3));

    public OtpRepository() {
    }

    public void createOtp(final Otp otp) {
        getExistingOtps().add(otp);
    }

    public void updateOtp(final int newOtp, final Integer userId) {
        final Otp otp4 = findOtpFromList(o -> o.getUserId() == userId);
        if (otp1 == null) {
            throw new OtpNotFoundException("OTP Not Found" + newOtp);
        } else {
            otp4.setOtpNumber(newOtp);
        }
    }

    public boolean isOtpWrong(final int otp, final Integer userId) {
        checkOtpListExists();
        final Otp otpFromList = findOtpFromList(o -> o.getOtpNumber() == otp);
        if (otpFromList == null || otpFromList.getUserId() != userId) {
            return true;
        }
        return false;
    }

    public boolean deleteOtp(final int otp) {
        final Otp otp5 = findOtpFromList(o -> o.getOtpNumber() == otp);
        if (otp5 != null) {
            getExistingOtps().remove(otp5);
        }
        return false;
    }

    private int countOtps() {
        return existingOtpList != null ? getExistingOtps().size() : 0;
    }

    private List<Otp> getExistingOtps() {
        return existingOtpList;
    }

    private Otp findOtpFromList(final Predicate<Otp> predicate) {
        checkOtpListExists();
        return getExistingOtps().stream().filter(predicate).findFirst().orElse(null);
    }

    private static boolean checkOtpListExists() {
        final boolean otpListExists = (existingOtpList != null && !existingOtpList.isEmpty());
        if (!otpListExists) {
            throw new RuntimeException("Otp list is null");
        }
        return true;
    }

    private boolean ifOtpListNullOrEmpty(final Otp otp) {
        if (existingOtpList == null || existingOtpList.isEmpty()) {
            existingOtpList = new ArrayList(Arrays.asList(otp));
            return true;
        }
        return false;
    }

}
