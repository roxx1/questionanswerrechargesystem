package com.company.repository;

import com.company.models.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserRepository {

    private static final User user1 = new User(1, 9999999999L, "abcdef", 12.34);
    private static final User user2 = new User(2, 9999988888L, "ghijkl", 16.75);
    private static final User user3 = new User(3, 9999977777L, "mnopqr", 72.36);
    private static final User user4 = new User(4, 9999966666L, "stuvwx", 91.14);
    private static final User user5 = new User(5, 9999955555L, "kingkong", 83.84);
    private static final List<User> existingUsers = new ArrayList<>(Arrays.asList(user1, user2, user3, user4, user5));

    public List<User> getExistingUsers() {
        return existingUsers;
    }

    public void createUser(final User user) {
        getExistingUsers().add(user);
    }

    public boolean removeUser(final User user) {
        if (existingUsers == null || existingUsers.isEmpty()) {
            throw new RuntimeException("USer list is Empty");
        }
        return getExistingUsers().remove(user);
    }

}
