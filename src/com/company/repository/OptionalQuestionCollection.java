package com.company.repository;

import com.company.models.OptionalQuestion;

import java.util.Arrays;
import java.util.List;

public class OptionalQuestionCollection implements IQuestionCollection<OptionalQuestion> {

    private static final OptionalQuestion o1 = new OptionalQuestion(1, "Q: What would you like to do?", new String[]{"a. Press 1 to Check Balance", "b. Press 2 Recharge"});
    private static final OptionalQuestion o2 = new OptionalQuestion(2, "Your OTP is invalid! ", new String[]{"Press 1 to re-enter", "Press 2 to re-generate"});

    @Override
    public List<OptionalQuestion> getQuestionForExistingUser() {
        return Arrays.asList(o1);
    }

    @Override
    public List<OptionalQuestion> getQuestionForNewUser() {
        return Arrays.asList(o2);
    }


}
